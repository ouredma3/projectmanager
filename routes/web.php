<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/projects', 'ProjectController@getProjects');

$router->get('/projects/{id}', 'ProjectController@getProject');

$router->post('/projects', 'ProjectController@addProject');

$router->delete('/projects/{id}', 'ProjectController@deleteProject');

$router->put('/projects/{id}', 'ProjectController@editProject');

$router->get('/projects/{id}/tasks', 'ProjectController@getTasks');



$router->get('/tasks', 'TaskController@getTasks');

$router->post('/tasks', 'TaskController@addTask');

$router->get('/tasks/{id}', 'TaskController@getTask');

$router->post('/tasks/{id}/add-time', 'TaskController@addTime');

$router->delete('/tasks/{id}', 'TaskController@deleteTask');

$router->put('/tasks/{id}', 'TaskController@editTask');