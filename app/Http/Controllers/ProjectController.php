<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 03/05/2020
 * Time: 15:37
 */

namespace App\Http\Controllers;

use App\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Attendance;
use App\Traits\ApiResponser;

class ProjectController extends Controller
{
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getProjects(){
        $projects = Project::all();
        return response()->json($projects);
    }

    public function addProject(Request $request){
        try {

            $this->validate($request, [
                'name' => 'required|string',
            ]);


            $project = new Project();

            $project->name = $request->input('name');

            $project->save();

            //return successful response
            return response()->json(['project' => $project, 'message' => 'SAVED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Project edit failed!'], 409);
        }
    }


    public function getProject($id){
        $project = Project::findOrFail($id);
        return response()->json($project);
    }

    public function getTasks($id){
        $project = Project::findOrFail($id);
        return response()->json($project->tasks);
    }


    public function editProject(Request $request, $id)
    {
        try {

            $this->validate($request, [
                'name' => 'required|string',
            ]);


            $project = Project::findOrFail($id);

            $project->name = $request->input('name');

            $project->save();

            //return successful response
            return response()->json(['project' => $project, 'message' => 'SAVED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Project edit failed!'], 409);
        }
    }

    /**
     * Delete project by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProject($id)
    {
        try {
            $project = Project::findOrFail($id);
            $project->delete();

            return response()->json(['message' => 'Project successfully deleted'], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Project not found!'], 404);
        }
    }


}