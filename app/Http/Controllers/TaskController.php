<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 03/05/2020
 * Time: 15:37
 */

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Attendance;
use App\Traits\ApiResponser;

class TaskController extends Controller
{
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getTasks(){
        $tasks = Task::all();
        return response()->json($tasks);
    }

    public function getTask($id){
        $Task = Task::findOrFail($id);
        return response()->json($Task);
    }

    public function addTask(Request $request){
        try {
            $this->validate($request, [
                'project_id' => 'required|integer',
                'name' => 'required|string',
                'estimate' => 'integer',
            ]);

            $project = Project::find($request->input('project_id'));
            $project->tasks()->create($request->all());
            return response()->json(['message' => 'CREATED'], 201);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Task create failed!'], 409);
        }

    }

    public function addTime(Request $request, $id)
    {
        try {

            $this->validate($request, [
                'amount' => 'required|integer',
            ]);


            $Task = Task::find($id);

            $Task->actual += $request->input('amount');

            $Task->save();

            //return successful response
            return response()->json(['Task' => $Task, 'message' => 'SAVED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Task edit failed!'], 409);
        }
    }

    public function editTask(Request $request, $id)
    {
        try {

            $this->validate($request, [
                'name' => 'required|string',
            ]);


            $Task = Task::find($id);

            $Task->name = $request->input('name');


            if($request->input('estimate') != '' &&  $request->input('estimate') != $Task->estimate){
                $this->validate($request, [
                    'estimate' => 'integer',
                ]);
                $Task->estimate = $request->input('estimate');
            }

            if($request->input('actual') != '' &&  $request->input('actual') != $Task->actual){
                $this->validate($request, [
                    'actual' => 'integer',
                ]);
                $Task->actual = $request->input('actual');
            }

            $Task->save();

            //return successful response
            return response()->json(['Task' => $Task, 'message' => 'SAVED'], 201);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'Task edit failed!'], 409);
        }
    }

    /**
     * Delete Task by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTask($id)
    {
        try {
            $Task = Task::find($id);
            $Task->delete();

            return response()->json(['message' => 'Task successfully deleted'], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Task not found!'], 404);
        }
    }
}